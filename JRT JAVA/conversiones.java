
public class conversiones {

	//movimiento rectilinieo uniforme
	public double aceleración (double vf, double vi, double t)
	{
		return (vf-vi)/t;
	}
	
	public double tiempo (double vf, double vi, double a)
	{
		return (vf-vi)/a;
	}
	
	public double velocidadFinal (double vi, double t, double a)
	{
		return a*t+vi;
	}
	
	public double velocidadInicial (double vf, double t, double a)
	{
		return a*t + vf;
	}

	//velocidad media
	public double velocidadMedia (double vf, double vi)
	{
		return (vi + vf)/2;
	}
	
	public double velocidadFinal (double vi, double vm)
	{
		return vm*2-vi;
	}
	
	public double velocidadInicial (double vf, double vm)
	{
		return vm*2 - vf;
	}
	
	
	//distancia
	public double distancia (double vf, double vi, double t)
	{
		return ((vi+vf)/2)*t;
	}
	
	public double tiempod (double vf, double vi, double d)
	{
		return d/((vi+vf)/2);
	}
	
	public double velocidadIniciald (double vf, double t, double d)
	{
		return (d*2/t)-vf;
	}
	
	public double velocidadFinald (double vi, double t, double d)
	{
		return (d*2/t)-vi;
	}
	
	//desde reposo
	public double distancia (double t, double a)
	{
		return (a*Math.pow(t, 2))/2;
	}
	
	public double tiempo (double d, double a)
	{
		return Math.sqrt((d*2)/a);
	}
	
	public double aceleracion (double d, double t)
	{
		return (d*2)/Math.sqrt(Math.pow(t, 2));
	}
	
	
	//aceleracion negativa
	public double distanciaVelocidadCero (double a, double t)
	{
		return (-a*Math.pow(t, 2))/2;
	}
	
	public double tiempoVelocidadCero (double d, double a)
	{
		return Math.sqrt((d*2)/a);
	}
	
	
	
	
}
