
import java.text.DecimalFormat;
import java.util.*;

public class funciones {
	private int cuadrante;
	public ArrayList<Double> compX = new ArrayList();
	public ArrayList<Double> compY = new ArrayList();
	String medida="";
	
	//calcula la distancia recorrida
	public double distancia(double catetoA, double catetoB){
		
		return catetoA + catetoB;
	}
	
	
	//Calcula el dezplazamiento
	public double desplazamiento(double catetoA, double catetoB){
		
		return Math.sqrt(Math.pow(catetoA, 2) + Math.pow(catetoB, 2));
	}
	
	
	//calcula el angulo 
	public double angulo (double catetoA, double catetoB){
		return Math.toDegrees(Math.atan2(Math.abs(catetoB), Math.abs(catetoA)));
	}
	
	 public String formatString (double number) {
		 
	        DecimalFormat f = new DecimalFormat("##.00");  // this will helps you to always keeps in two decimal places
	        return(f.format(number));
	    }
	
	public void cuadrante (String dir, String dir2){
		if (dir.equalsIgnoreCase("N") && dir2.equalsIgnoreCase("E"))
			cuadrante = 1;
		if (dir.equalsIgnoreCase("E") && dir2.equalsIgnoreCase("N"))
			cuadrante = 2;
		if (dir.equalsIgnoreCase("N") && dir2.equalsIgnoreCase("O"))
			cuadrante = 3;
		if (dir.equalsIgnoreCase("O") && dir2.equalsIgnoreCase("N"))
			cuadrante = 4;
		if (dir.equalsIgnoreCase("S") && dir2.equalsIgnoreCase("O"))
			cuadrante = 5;
		if (dir.equalsIgnoreCase("O") && dir2.equalsIgnoreCase("S"))
			cuadrante = 6;
		if (dir.equalsIgnoreCase("S") && dir2.equalsIgnoreCase("E"))
			cuadrante = 7;
		if (dir.equalsIgnoreCase("E") && dir2.equalsIgnoreCase("S"))
			cuadrante = 8;
		if (dir.equalsIgnoreCase("N") && dir2.equalsIgnoreCase("S"))
			cuadrante = 9;
		if (dir.equalsIgnoreCase("S") && dir2.equalsIgnoreCase("N"))
			cuadrante = 10;
		if (dir.equalsIgnoreCase("E") && dir2.equalsIgnoreCase("O"))
			cuadrante = 11;
		if (dir.equalsIgnoreCase("O") && dir2.equalsIgnoreCase("E"))
			cuadrante = 12;
		if (dir.equalsIgnoreCase("N") && dir2.equalsIgnoreCase("N"))
			cuadrante = 13;
		if (dir.equalsIgnoreCase("E") && dir2.equalsIgnoreCase("E"))
			cuadrante = 14;
		if (dir.equalsIgnoreCase("S") && dir2.equalsIgnoreCase("S"))
			cuadrante = 15;
		if (dir.equalsIgnoreCase("O") && dir2.equalsIgnoreCase("O"))
			cuadrante = 16;
		if (dir.equalsIgnoreCase("N") && dir2.equalsIgnoreCase(""))
			cuadrante = 17;
		if (dir.equalsIgnoreCase("E") && dir2.equalsIgnoreCase(""))
			cuadrante = 18;
		if (dir.equalsIgnoreCase("S") && dir2.equalsIgnoreCase(""))
			cuadrante = 19;
		if (dir.equalsIgnoreCase("O") && dir2.equalsIgnoreCase(""))
			cuadrante = 20;
	}
	
	public String dezplazamientoVectorial (double catetoA, double catetoB, String dir, String dir2){
		double desp = desplazamiento(catetoA, catetoB);
		double alpha = angulo(catetoA, catetoB);
		cuadrante(dir, dir2);
		switch(cuadrante){
		 case 1: 
		     return "Se desplaz� " + formatString (desp) + medida +" Norte con un �ngulo de " + formatString (alpha) + " hacia el Este \n" +
		    		 "Se desplaz� " + formatString (desp) + medida +" Este con un �ngulo de " + formatString (90- alpha) + " hacia el Norte";
		     
		 case 2:
			 return "Se desplaz� " + formatString (desp) + medida +" Este con un �ngulo de " + formatString (alpha) + " hacia el Norte \n" +
    		 		"Se desplaz� " + formatString (desp) + medida +" Norte con un �ngulo de " + formatString (90- alpha) + " hacia el Este";
			 
		 case 3: 
		     return "Se desplaz� " + formatString (desp) + medida +" Norte con un �ngulo de " + formatString (alpha) + " hacia el Oeste \n" +
		    		 "Se desplaz� " + formatString (desp) + medida +" Oeste con un �ngulo de " + formatString (90- alpha) + " hacia el Norte";
		     
		 case 4:
			 return "Se desplaz� " + formatString (desp) + medida +" Oeste con un �ngulo de " + formatString (alpha) + " hacia el Norte \n" +
    		 		"Se desplaz� " + formatString (desp) + medida +" Norte con un �ngulo de " + formatString (90- alpha) + " hacia el Oeste";
			 
		 case 5: 
		     return "Se desplaz� " + formatString (desp) + medida +" Sur con un �ngulo de " + formatString (alpha) + " hacia el Oeste \n" +
		    		 "Se desplaz� " + formatString (desp) + medida +" Oeste con un �ngulo de " + formatString (90- alpha) + " hacia el Sur";
		    
		 case 6:
			 return "Se desplaz� " + formatString (desp) + medida +" Oeste con un �ngulo de " + formatString (alpha) + " hacia el Sur \n" +
    		 		"Se desplaz� " + formatString (desp) + medida +" Sur con un �ngulo de " + formatString (90- alpha) + " hacia el Oeste";
			
		 case 7: 
		     return "Se desplaz� " + formatString (desp) + medida +" Sur con un �ngulo de " + formatString (alpha) + " hacia el Este \n" +
		    		 "Se desplaz� " + formatString (desp) + medida +" Este con un �ngulo de " + formatString (90- alpha) + " hacia el Sur";
		    
		 case 8:
			 return "Se desplaz� " + formatString (desp) + medida +" Este con un �ngulo de " + formatString (alpha) + " hacia el Sur \n" +
    		 		"Se desplaz� " + formatString (desp) + medida +" Sur con un �ngulo de " + formatString (90- alpha) + " hacia el Este";
		 case 9:
			 if ((catetoA - catetoB) > 0)
				 return "Se desplaz� " + (catetoA-catetoB) + medida +" hacia el Norte";
			 else if((catetoA - catetoB) < 0)
			 	return "Se desplaz� " + (catetoA-catetoB)*-1 + medida +" hacia el Sur";
			 	else
			 		return "No hubo desplazamiento";
		 case 10:
			 if ((catetoA - catetoB) > 0)
				 return "Se desplaz� " + (catetoA-catetoB) + medida +" hacia el Sur";
			 else if((catetoA - catetoB) < 0)
			 	return "Se desplaz� " + (catetoA-catetoB)*-1 + medida +" hacia el Norte";
			 	else
			 		return "No hubo desplazamiento";
		 case 11:
			 if ((catetoA - catetoB) > 0)
				 return "Se desplaz� " + (catetoA-catetoB) + medida +" hacia el Este";
			 else if((catetoA - catetoB) < 0)
			 	return "Se desplaz� " + (catetoA-catetoB)*-1 + medida +" hacia el Oeste";
			 	else
			 		return "No hubo desplazamiento";
		 case 12:
			 if ((catetoA - catetoB) > 0)
				 return "Se desplaz� " + (catetoA-catetoB) + medida +" hacia el Oeste";
			 else if((catetoA - catetoB) < 0)
			 	return "Se desplaz� " + (catetoA-catetoB)*-1 + medida +" hacia el Este";
			 	else
			 		return "No hubo desplazamiento";
		 case 13:
			 return "Se desplaz� " + (catetoA + catetoB) + medida +" hacia el Norte";
			 
		 case 14:
			 return "Se desplaz� " + (catetoA + catetoB) + medida +" hacia el Este";
			 
		 case 15:
			 return "Se desplaz� " + (catetoA + catetoB) + medida +" hacia el Sur";
			 
		 case 16:
			 return "Se desplaz� " + (catetoA + catetoB) + medida +" hacia el Oeste";
		 
		 default: 
		     return "";
		}
	}
	
	public void componentesCuadrantes2 (double componente, double angulo, String dir1, String dir2)
	{
		cuadrante(dir1, dir2);
		switch(cuadrante){
		 case 1: 
		     Ax1(componente, 90-angulo);
		     Ay1(componente, 90-angulo);
		     break;
		 case 2: 
		     Ax1(componente, angulo);
		     Ay1(componente, angulo);
		     break;
		 case 3: 
		     Ax2(componente, 90-angulo);
		     Ay2(componente, 90-angulo);
		     break;
		 case 4: 
		     Ax2(componente, angulo);
		     Ay2(componente, angulo);
		     break;
		 case 5: 
		     Ax3(componente, 90-angulo);
		     Ay3(componente, 90-angulo);
		     break;
		 case 6: 
		     Ax3(componente, angulo);
		     Ay3(componente, angulo);
		     break;
		 case 7: 
		     Ax4(componente, 90-angulo);
		     Ay4(componente, 90-angulo);
		     break;
		 case 8: 
		     Ax4(componente, angulo);
		     Ay4(componente, angulo);
		     break;
		 case 17: 
			 compX.add(0.0);
		     Ay1(componente, 90);
		     break;
		 case 18: 
		     Ax1(componente, 0);
		     compX.add(0.0);
		     break;
		 case 19: 
			 compX.add(0.0);;
		     Ay1(componente, 270);
		     break;
		 case 20: 
		     Ax1(componente, 180);
		     compX.add(0.0);
		     break;
		}
	}
	
	
	public String sumDespVectorial (double catetoA, double catetoB)
	{
		String dir1="";
		String dir2="";
		if (catetoA>0 && catetoB>0)
		{
			dir1 = "E";
			dir2 = "N";
		}
		if (catetoA<0 && catetoB>0)
		{
			dir1 = "O";
			dir2 = "N";
		}
		if (catetoA<0 && catetoB<0)
		{
			dir1 = "O";
			dir2 = "S";
		}
		if (catetoA>0 && catetoB<0)
		{
			dir1 = "E";
			dir2 = "S";
		}
		return dezplazamientoVectorial(catetoA, catetoB, dir1, dir2);
	}
	
	public double Ax1 (double componente, double angulo)
	{
		compX.add(componente*Math.cos(Math.toRadians(angulo)));
		double	ax =componente*Math.cos(Math.toRadians(angulo));
		System.out.println(ax);
		return ax;
	}
	
	public double Ay1 (double componente, double angulo)
	{
		compY.add(componente*Math.sin(Math.toRadians((angulo))));
		double ay=componente*Math.sin(Math.toRadians((angulo)));
		return ay;
	}
	public double Ax2 (double componente, double angulo)
	{
		compX.add(componente*Math.cos(Math.toRadians(180-angulo)));
		double ax=componente*Math.cos(Math.toRadians(180-angulo));
		return ax;
	}
	
	public double Ay2 (double componente, double angulo)
	{
		compY.add(componente*Math.cos(Math.toRadians(90-(180-angulo))));
		double ay= componente*Math.cos(Math.toRadians(90-(180-angulo)));
		return ay;
	}
	public double Ax3 (double componente, double angulo)
	{
		compX.add(componente*Math.cos(Math.toRadians(180+angulo)));
		double ax=componente*Math.cos(Math.toRadians(180+angulo));
		return ax;
	}
	
	public double Ay3 (double componente, double angulo)
	{
		compY.add(componente*Math.cos(Math.toRadians(90-(180+angulo))));
		double ay= componente*Math.cos(Math.toRadians(90-(180+angulo)));
		return ay;
	}
	public double Ax4 (double componente, double angulo)
	{
		compX.add(componente*Math.cos(Math.toRadians(360-angulo)));
		double ax=componente*Math.cos(Math.toRadians(360-angulo));
		return ax;
	}
	
	public double Ay4 (double componente, double angulo)
	{
		compY.add(componente*Math.cos(Math.toRadians(90-(360-angulo))));
		double ay= componente*Math.cos(Math.toRadians(90-(360-angulo)));
		return ay;
	}


	
	public String componentesCuadrantes (int cuadrante, double componente, double angulo, String medid)
	{
		medida = medid;
		switch(cuadrante){
		 case 1:
			 return "Ax: " + formatString(Ax1(componente, angulo))+ " " + medida + "\nAy: " + formatString(Ay1(componente, angulo))+ " " + medida;
		 case 2:
			 return "Ax: " + formatString(Ax2(componente, angulo))+ " " + medida +"\nAy: " + formatString(Ay2(componente, angulo))+ " " + medida;
		 case 3:
			 return "Ax: " + formatString(Ax3(componente, angulo))+ " " + medida +"\nAy: " + formatString(Ay3(componente, angulo))+ " " + medida;
		 case 4:
			 return "Ax: " + formatString(Ax4(componente, angulo))+ " " + medida +"\nAy: " + formatString(Ay4(componente, angulo)) + " " + medida;
		default:
			return "";
		}
		
	}
	
	public double sumatorioX ()
	{
		double result = 0;
		for(int i=0; i<compX.size(); i++)
		{
			result += compX.get(i);
		}
		return (double) Math.round(result * 100) / 100;
		
	}
	
	public double sumatorioY ()
	{
		double result = 0;
		for(int i=0; i<compY.size(); i++)
		{
			result += compY.get(i);
		}
		return (double) Math.round(result * 100) / 100;
	}
	
	public static void main(String [] args)
	{
		funciones f = new funciones ();
		System.out.println(f.distancia(100, 200));
		System.out.println(f.dezplazamientoVectorial(100, 200, "E", "N"));
		f.componentesCuadrantes2 (825, 270, "S", "");
		f.componentesCuadrantes2 (1500, 30, "N","O");
		f.componentesCuadrantes2 (1000, 40, "N","O");
		System.out.println(f.sumDespVectorial(f.sumatorioX(), f.sumatorioY()));
		System.out.println(f.compX.toString());
		System.out.println(f.compY.toString());
		System.out.println(f.sumatorioX());
		System.out.println(f.sumatorioY());
		System.out.println(f.componentesCuadrantes(1,150.00, 58,"M"));
		
		
		
	}
}

